%undefine __cmake_in_source_build
%global framework kcrash

Name:          kf5-%{framework}
Version:       5.116.0
Release:       1
Summary:       KDE Frameworks 5 Tier 2 addon for handling application crashes

License:       LGPLv2+
URL:           https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

## upstream patches

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-kcoreaddons-devel >= %{majmin}
BuildRequires:  kf5-kwindowsystem-devel >= %{majmin}
BuildRequires:  kf5-rpm-macros

BuildRequires:  libX11-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtx11extras-devel

%description
KCrash provides support for intercepting and handling application crashes.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DKCRASH_CORE_PATTERN_RAISE:BOOL=OFF

%cmake_build

%install
%cmake_install

%ldconfig_scriptlets

%files
%doc README.md
%license LICENSES/*.txt
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_libdir}/libKF5Crash.so.*

%files devel
%{_kf5_includedir}/KCrash/
%{_kf5_libdir}/libKF5Crash.so
%{_kf5_libdir}/cmake/KF5Crash/
%{_kf5_archdatadir}/mkspecs/modules/qt_KCrash.pri


%changelog
* Mon Jan 20 2025 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- Update package to version 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 04 2024 huayadong <huayadong@kylinos.cn> - 5.115.0-1
- Update package to version 5.115.0

* Wed Jan 03 2024 wangqia <wangqia@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Thu Aug 03 2023 zhangshaoning <zhangshaoning@uniontech.com> - 5.108.0-1
- Update to upstream version 5.108.0

* Mon Dec 12 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 5.100.0-1
- Update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 liqiuyu <liqiuyu@kylinos.cn> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 tanyulong <tanyulong@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Sun Jan 16 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Aug 17 2020 yeqinglong <yeqinglong@uniontech.com> - 5.55.0-1
- Initial release for OpenEuler
